﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapPartManager : MonoBehaviour {

    public List<GameObject> mapParts;

	void Start () {
        
	}
	
	
	void Update () {
        for (int i = 0; i < mapParts.Count; i++)
        {
            if (mapParts[i] != null)
            {
                if (mapParts[i].transform.position.z < Camera.main.transform.position.z - 5)
                {
                    Destroy(mapParts[i]);
                } 
            }
        }
	}
}
