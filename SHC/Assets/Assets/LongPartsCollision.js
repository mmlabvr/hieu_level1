﻿#pragma strict
var before :GameObject;
var before_bar:GameObject;
var grandparent : GameObject;
var after_parent :GameObject;
var after_subparent : GameObject;
var tr_grandparent :Transform;
var tr_after_parent:Transform;
var tr_after_subparent :Transform;
var number:int;
var particle_isPlayed:boolean;

function Start(){
	tr_after_subparent = this.gameObject.transform.parent;  
	tr_after_parent= tr_after_subparent.transform.parent;
	tr_grandparent = tr_after_parent.transform.parent;
	grandparent = tr_grandparent.gameObject;
	before = tr_grandparent.FindChild("Longboard_before_parts").gameObject;
	before_bar = before.transform.FindChild("Longboard_before").gameObject;
	after_parent= tr_after_parent.gameObject;
	after_subparent = tr_after_subparent.gameObject;
}

function OnCollisionEnter(col:Collision){
	if(col.gameObject.tag == "cannonball"){
		before_bar.GetComponent.<Renderer>().enabled = false;
		//represent renderer for all parts
		for(var i:int =0;i<tr_after_parent.childCount;i++){
			var sub:Transform = tr_after_parent.GetChild(i);
			for(var j:int=0;j<sub.childCount;j++){
				var tr = sub.GetChild(j);
				tr.gameObject.GetComponent.<Renderer>().enabled = true;
			}
		}
		//get this category
		var parent_name:String = after_subparent.name;
		var number_str :String = parent_name.Substring(5,1); 
		var number:int = parseInt(number_str);
		//get isPlayed flg from controller
		var HoleLongBarController : HoleLongBarController = after_parent.gameObject.GetComponent("HoleLongBarController");
		var particle_isPlayed = HoleLongBarController.GetisPlayed(number-1);
		
		var pos:Vector3 = col.gameObject.transform.position;
		var dir:Vector3 = col.gameObject.transform.forward;
		if(particle_isPlayed == false){
			StartingParticle(pos,dir,number);
		}
		AddForceToParts(pos  ,dir);
		after_parent.gameObject.SendMessage("ManagementBar",number-1);
		
	}
}

function AddForceToParts(p:Vector3  , dir:Vector3){
	for(var i:int =0;i<tr_after_subparent.childCount;i++){
		var child:Transform = tr_after_subparent.GetChild(i);
		child.gameObject.GetComponent.<Rigidbody>().isKinematic = false;
		child.gameObject.GetComponent.<Rigidbody>().AddExplosionForce(50.0f,p,1.0f);
		child.gameObject.GetComponent.<Rigidbody>().AddForce(dir*20);
		Destroy(child.gameObject,2.0);		
	}
}

function StartingParticle(p:Vector3,a:Vector3,n:int){
		var ps:GameObject =	tr_grandparent.FindChild("Particle_System_for_bar"+n.ToString()).gameObject;
		ps.transform.position = p;
		ps.transform.forward = a;
		ps.SetActive(true);
		ps.transform.parent = null;
		Destroy(ps,2.0);	
}




