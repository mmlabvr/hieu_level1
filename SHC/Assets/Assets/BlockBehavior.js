﻿#pragma strict

var speed : float = 1.0f;
var p: Vector3;
var flg : boolean;

function Start () {
	p = this.transform.localPosition;
}

function Update () {
	if(flg == false){
		p.y -= speed*Time.deltaTime;
		this.transform.localPosition = p;
		if(p.y <= -1){
			flg = true;
		}
	} else {
		p.y += speed*Time.deltaTime;
		this.transform.localPosition = p;
		if(p.y >= 1){
			flg = false;
		}
	}
}
