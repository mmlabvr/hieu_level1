﻿#pragma strict
var sound01:AudioClip;
var sound02:AudioClip;
var sound03:AudioClip;
private var audioSource:AudioSource;

function Start () {
	
}

function StartCannonSE(){
	audioSource = gameObject.GetComponent(AudioSource);
	audioSource.clip = sound02;
	audioSource.Play();
}

function StartObstacleSE(){
	audioSource = gameObject.GetComponent(AudioSource);
	audioSource.clip = sound03;
	audioSource.Play();
}

function StartGameOverSE(){
	audioSource = gameObject.GetComponent(AudioSource);
	audioSource.clip = sound01;
	audioSource.Play();
}
