﻿#pragma strict
var sound:AudioClip;
private var audioSource:AudioSource;

function OnCollisionEnter(col:Collision){
//	Debug.Log("called");
	if(col.gameObject.tag == "cannonball"){
		audioSource = gameObject.GetComponent(AudioSource);
		audioSource.clip = sound;
		audioSource.Play();
	}
}
