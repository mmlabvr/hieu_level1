﻿#pragma strict
var PauseICon : Texture;
var StartICon : Texture;
var Cannon : GameObject;
var Player : GameObject;
var IsPause : boolean;
function Start(){
	Cannon = GameObject.Find("Cannon").gameObject;
	Player = GameObject.Find("Player").gameObject;
	this.GetComponent.<GUITexture>().texture = PauseICon;
	IsPause = false;
}

function OnMouseOver(){
	
}

function OnMouseExit(){
	
}

function OnMouseDown(){
	if(IsPause == false){
		Time.timeScale = 0.0f;
		IsPause = true;
		this.GetComponent.<GUITexture>().texture = StartICon;
		Cannon.SetActive(false);
		Player.GetComponent.<AudioSource>().Pause();
	} else {
		Time.timeScale = 1.0f;
		IsPause = false;
		this.GetComponent.<GUITexture>().texture = PauseICon;
		Player.GetComponent.<AudioSource>().Play();
		yield;
		Cannon.SetActive(true);
	}
}

