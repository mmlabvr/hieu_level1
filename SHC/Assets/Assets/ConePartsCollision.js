﻿#pragma strict
var parent : GameObject;
var after_parent : GameObject;
var tr :Transform;
var smallfraction : GameObject;
var Particle_isPlayed :boolean;

function Start(){
	var transform : Transform = parent.transform.FindChild("Particle System_for_cone");
	Particle_isPlayed =false;
//	smallfraction = transform.gameObject;
}

function OnCollisionEnter(col:Collision){
	if(col.gameObject.tag == "cannonball"){
		parent.gameObject.SendMessage("ManagementCone");
		var dir:Vector3 = col.gameObject.transform.position;
		AddForceToParts(dir);
		if(Particle_isPlayed == false){
			smallfraction.SetActive(true);
			Particle_isPlayed = true;
		}
	}
}

function AddForceToParts(p:Vector3){
	tr = after_parent.transform;
	yield;
	for(var i:int =0;i<tr.childCount;i++){
		var child:Transform = tr.GetChild(i); 
		child.gameObject.GetComponent.<Rigidbody>().AddExplosionForce(70.0f,p,1.0f);
	}
}
