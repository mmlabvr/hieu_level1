﻿#pragma strict
var Sub : GameObject[];
var IsFalled : boolean[];
var before :GameObject;
var sound01:AudioClip;
private var audioSource:AudioSource;

function Start () {
	before = this.transform.parent.transform.FindChild("Longboard_before_parts").gameObject;
	Sub = new GameObject[this.transform.childCount];
	IsFalled = new boolean[this.transform.childCount];
	for(var i=0;i<this.transform.childCount;i++){
		Sub[i] = this.transform.GetChild(i).gameObject;
		IsFalled[i] = false;
	}
}

function ManagementBar (n:int) {
	if(IsFalled[n] == false){
//	Debug.Log("called");
	PlayBarClashSound();
	IsFalled[n] = true;
	if(n>0 && n<7){
		if(IsFalled[n-1]==true || IsFalled[n+1]==true){
			BreakAllParts(n+1);
		}
	}
	if(n==0 && IsFalled[1]==true){
		BreakAllParts(2);
	}
	if(IsFalled[0]==true && IsFalled[3]==true){
		BreakAllParts(2);
	}
	if(IsFalled[2]==true && IsFalled[5]==true){
		BreakAllParts(4);
	}
	if(IsFalled[4]==true && IsFalled[7]==true){
		BreakAllParts(6);
	}
//	Debug.Log(IsFalled[0]+","+IsFalled[1]+","+IsFalled[2]+","+IsFalled[3]+","+IsFalled[4]+","+IsFalled[5]+","+IsFalled[6]+","+IsFalled[7]);

	if(IsFalled[4]==true || IsFalled[5]==true || IsFalled[6]==true || IsFalled[7]==true){
		before.SetActive(false);
	}
	SepareteChildrenFromParent();
	}
}


function BreakAllParts (start:int){
	for(var k:int=start;k<this.transform.childCount;k++){
		IsFalled[k] = true;
		var tr : Transform = Sub[k].transform;
		for(var l:int=0;l<tr.childCount;l++){
			Sub[k].transform.GetChild(l).gameObject.GetComponent.<Rigidbody>().isKinematic = false;
			Sub[k].transform.GetChild(l).gameObject.GetComponent.<Rigidbody>().AddForce(Vector3(0,-1,10));
		}
	}
}


function GetisPlayed(n:int){
	return IsFalled[n];
}

function SepareteChildrenFromParent(){
	for(var n=0;n<this.transform.childCount;n++){
		if(IsFalled[n]==true){
			var num:int = Sub[n].transform.childCount;
			for(var i:int =0;i<num;i++){
				var child:Transform = Sub[n].transform.GetChild(0);
				child.transform.parent = null;
				Destroy(child.gameObject,2.0);		
			}
		}
	}
}

function PlayBarClashSound(){
	audioSource = gameObject.GetComponent(AudioSource);
	audioSource.clip = sound01;
	audioSource.Play();
}