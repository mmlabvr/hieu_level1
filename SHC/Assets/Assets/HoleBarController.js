﻿#pragma strict
var Sub : GameObject[];
var IsFalled : boolean[];
var before :GameObject;
var sound01:AudioClip;
private var audioSource:AudioSource;


function Start () {
	before = this.transform.parent.transform.FindChild("before_parts").gameObject;
	for(var i=0;i<this.transform.childCount;i++){
		Sub[i] = this.transform.GetChild(i).gameObject;
		IsFalled[i] = false;
	}
}

function ManagementBar (n:int) {
	if(IsFalled[n]==false){
		PlayBarClashSound();
	}
	IsFalled[n] = true;
	for(var i:int=0;i<n-1;i++){
		if(IsFalled[i] == true){
			BreakParts(i,n);
		}
	}
	for(var j:int=n+1;j<this.transform.childCount;j++){
		if(IsFalled[j] == true){
			BreakParts(n,j);
		}
	}
//	Debug.Log(IsFalled[0]+","+IsFalled[1]
//	+","+IsFalled[2]+","+IsFalled[3]+","+IsFalled[4]+","+IsFalled[5]);
	if(IsFalled[2]==true || IsFalled[3]==true){
		before.SetActive(false);
	}
}


function BreakParts (start:int,end:int){
//	Debug.Log("called:"+start+end);
	for(var k:int=start+1;k<end;k++){
		IsFalled[k] = true;
		var tr : Transform = Sub[k].transform;
		for(var l:int=0;l<tr.childCount;l++){
			Sub[k].transform.GetChild(l).gameObject.GetComponent.<Rigidbody>().isKinematic = false;
			Sub[k].transform.GetChild(l).gameObject.GetComponent.<Rigidbody>().AddForce(Vector3(0,-1,10));
		}
	}
}


function GetisPlayed(n:int){
	return IsFalled[n];
}

function PlayBarClashSound(){
	audioSource = gameObject.GetComponent(AudioSource);
	audioSource.clip = sound01;
	audioSource.Play();
}
