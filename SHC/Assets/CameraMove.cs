﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CameraMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
        DOTween.Init();
	}
	
	// Update is called once per frame
	void Update () {
        Camera.main.transform.DOMoveZ(Camera.main.transform.position.z + 1, 1.5f);
	}
}
