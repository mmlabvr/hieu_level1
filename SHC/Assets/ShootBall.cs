﻿using UnityEngine;
using System.Collections;

public class ShootBall : MonoBehaviour {
    public GameObject ball;
    public float ShotForce = 10000.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float x = ray.direction.x * ShotForce;
            float y = ray.direction.y * ShotForce;
            float z = ray.direction.z * ShotForce;

            GameObject go = Instantiate(ball, Camera.main.ScreenToWorldPoint(Input.mousePosition), Quaternion.identity) as GameObject;
            go.GetComponent<Rigidbody>().AddForce(new Vector3(x, y, z));
        }
	}
}
